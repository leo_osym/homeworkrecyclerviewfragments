package com.example.homeworkcontactlist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class DetailsActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        getActionBar()?.setDisplayHomeAsUpEnabled(true)
    }
}
