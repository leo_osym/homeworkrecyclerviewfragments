package com.example.homeworkcontactlist


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment : Fragment() {

    private var entry_id = -1
    lateinit var contacts: ArrayList<Contact>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        contacts = Contacts.getAllContacts()
        if(activity!!.intent.hasExtra("ID")) {
            entry_id = activity!!.intent.extras!!.get("ID") as Int
            loadDataToFragment(entry_id)
        } else {
            userName.text = ""
            userEmail.text = ""
            userAge.text = ""
            userCity.text = ""
            userDetails.text = ""
        }
    }

    fun loadDataToFragment(index: Int){
        userName.text = contacts[index].FullName
        userImage.setImageResource(contacts[index].ImageId ?: R.drawable.icon_1)
        userEmail.text = contacts[index].Email
        userAge.text = "Age: ${contacts[index].Age}"
        userCity.text = "City: ${contacts[index].City}"
        userDetails.text = contacts[index].Details
    }


}
