package com.example.homeworkcontactlist

class Contact(private var userName: String,
              private var userEmail: String,
              private var image: Int?,
              private var userAge: Int?,
              private var userCity: String?,
              private var userDetails: String?) {

    var FullName: String
        get() = userName
        set(newValue) {
            userName = newValue
        }

    var Email: String
        get() = userEmail
        set(newValue) {
            userEmail = newValue
        }

    var Age: Int?
        get() = userAge
        set(newValue) {
            userAge = newValue
        }

    var ImageId: Int?
        get() = image
        set(newValue) {
            image = newValue
        }

    var City: String?
        get() = userCity
        set(newValue) {
            userCity = newValue
        }

    var Details: String?
        get() = userDetails
        set(newValue) {
            userDetails = newValue
        }
}

class Contacts {

    companion object {

        private var Contacts: ArrayList<Contact> = ArrayList()
        init{
            Contacts.add(Contact("John Cena", "john.cena@i.ua", R.drawable.icon_1,
                41, "New York", "Champ"))
            Contacts.add(Contact("Axel Primeiro", "axel.primeiro@i.ua", R.drawable.icon_2,
                26, "Dnipro", "Someone I don't know"))
            Contacts.add(Contact("Chloe Price", "chloe.price@ex.ua",R.drawable.icon_3,
                19, "Arcadia Bay", "Cool chick"))
            Contacts.add(Contact("Maxine Caulfield", "max.the.great@i.ua",  R.drawable.icon_4,
                19, "Arcadia Bay", "Student"))
            Contacts.add(Contact("Mark Jefferson", "mark.jefferson@ex.ua",R.drawable.icon_5,
                45, "Arcadia Bay", "Photography teacher"))
            Contacts.add(Contact("Dean Moriarty", "dean.moriarty@i.ua",null,
                25, "Denver", "Traveller and adventurer"))
        }
        fun getAllContacts(): ArrayList<Contact> {
            return Contacts
        }
    }
}