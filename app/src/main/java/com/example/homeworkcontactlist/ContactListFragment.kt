package com.example.homeworkcontactlist

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_contact_list.*


class ContactListFragment : Fragment() {

    lateinit var contacts: ArrayList<Contact>
    lateinit var adapter: ContactsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_contact_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        contacts = Contacts.getAllContacts()
        adapter = ContactsAdapter(contacts, context!!)

        // add separator line
        activity!!.recyclerView.addItemDecoration(
            DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        )
        activity!!.recyclerView.layoutManager = LinearLayoutManager(activity)

        adapter.setListener(object : ContactsAdapter.Listener {
            override fun onClick(position: Int) {
                callDetailsFragment(position)
            }
        })
        activity!!.recyclerView.adapter = adapter
    }

    fun callDetailsFragment(position: Int){
        if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            val intent = Intent(activity, DetailsActivity::class.java)
            intent.putExtra("ID", position)
            startActivity(intent)
        } else {
            val detailsFragment = fragmentManager!!.findFragmentById(R.id.details_fragment) as DetailsFragment
            detailsFragment.loadDataToFragment(position)
        }

    }


}
